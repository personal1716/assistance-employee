import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatPaginatorIntl, MatPaginatorModule } from '@angular/material/paginator';
import { PaginatorIntService } from './paginator-int.service';

@NgModule({
  declarations: [],
  imports: [CommonModule, MatPaginatorModule],
  providers: [
    { provide: MatPaginatorIntl, useClass: PaginatorIntService },
  ],
})
export class PaginatorIntModule { }

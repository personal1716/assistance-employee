import { Injectable } from '@angular/core';
import { MatPaginatorIntl } from '@angular/material/paginator';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PaginatorIntService implements MatPaginatorIntl {

  constructor() { }

  changes = new Subject<void>();

  firstPageLabel = `Primera página`;
  itemsPerPageLabel = `Resultados por página:`;
  lastPageLabel = `Última página`;

  nextPageLabel = 'Siguiente página';
  previousPageLabel = 'Anterior página';

  getRangeLabel(page: number, pageSize: number, length: number): string {
    if (length === 0) {
      return `Pág 1 de 1`;
    }
    const amountPages = Math.ceil(length / pageSize);
    return `Pág ${page + 1} de ${amountPages}`;
  }
}

import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FormQuestionDatePickerType } from '../commons/FormQuestionDatePickerType';

@Component({
  selector: 'app-form-date-picker',
  templateUrl: '../view/form-date-picker.component.html',
  styleUrls: ['../view/form-date-picker.component.scss']
})
export class FormDatePickerComponent implements OnInit {

  @Input() form!: FormGroup;

  @Input()
  question!: FormQuestionDatePickerType;

  constructor() { }

  ngOnInit(): void {
  }

}

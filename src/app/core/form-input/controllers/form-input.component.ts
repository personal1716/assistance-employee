import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FormQuestionInputType, TypePattern } from '../commons/FormQuestionInputType';

@Component({
  selector: 'app-form-input',
  templateUrl: '../views/form-input.component.html',
  styleUrls: ['../views/form-input.component.scss']
})
export class FormInputComponent implements OnInit {

  @Input() form!: FormGroup;

  @Input()
  question!: FormQuestionInputType;

  @Input() type: "text" | "number" = "text";

  @Input()
  min: number = 0;

  @Input()
  max!: number;

  @Input()
  uppercase: boolean = false;

  constructor() { }

  ngOnInit(): void {
  }

  toUpperCase(value: string) {
    if (!this.uppercase) return;
    this.form.get(this.question.key)?.setValue(value.toUpperCase());
  }
}

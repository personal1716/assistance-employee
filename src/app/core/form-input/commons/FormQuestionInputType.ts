export interface FormQuestionInputType {
  value: any;
  key: string;
  label: string;
  required?: boolean;
}

export type TypePattern = 'number' | 'number_az' | 'number_az_space' | 'number_az_minus';
import { Injectable } from '@angular/core';
import { AbstractControl, ValidationErrors } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class CustomValidationsService {

  constructor() { }

  onlyAZ(control: AbstractControl): ValidationErrors | null {
    const flag = /^[A-Z]*$/.test(control.value);
    return flag ? null : { onlyAZ: true };
  }

  onlyAZSpace(control: AbstractControl): ValidationErrors | null {
    const flag = /^[A-Z ]*$/.test(control.value);
    return flag ? null : { onlyAZSpace: true };
  }

  onlyNumberAZMinus(control: AbstractControl): ValidationErrors | null {
    const flag = /^[A-Z0-9-]*$/.test(control.value);
    return flag ? null : { onlyNumberAZMinus: true };
  }
}

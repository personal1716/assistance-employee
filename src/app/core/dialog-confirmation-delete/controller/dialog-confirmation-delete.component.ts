import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DialogConfirmationDeleteService } from '../common/dialog-confirmation-delete.service';

@Component({
  selector: 'app-dialog-confirmation-delete',
  templateUrl: '../view/dialog-confirmation-delete.component.html',
  styleUrls: ['../view/dialog-confirmation-delete.component.scss']
})
export class DialogConfirmationDeleteComponent implements OnInit {

  msg: string = "¿Está seguro(a) de realizar el proceso?";

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private dialogConfirmationDeleteService: DialogConfirmationDeleteService
  ) { }

  ngOnInit(): void {
    this.msg = this.data.msg;
  }

  closeModal(flag: boolean = false) {
    this.dialogConfirmationDeleteService.close(flag);
  }
}

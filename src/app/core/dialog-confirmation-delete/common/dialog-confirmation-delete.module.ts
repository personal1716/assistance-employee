import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MatDialogModule } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { DialogConfirmationDeleteComponent } from '../controller/dialog-confirmation-delete.component';
import { DialogConfirmationDeleteService } from './dialog-confirmation-delete.service';

@NgModule({
  declarations: [DialogConfirmationDeleteComponent],
  imports: [
    CommonModule,
    MatDialogModule,
    MatButtonModule,
  ], exports: [DialogConfirmationDeleteComponent],
  providers: [DialogConfirmationDeleteService]
})
export class DialogConfirmationDeleteModule { }

import { Injectable } from '@angular/core';
import { MatDialog, MatDialogConfig, MatDialogRef } from '@angular/material/dialog';
import { DialogConfirmationDeleteComponent } from '../controller/dialog-confirmation-delete.component';

@Injectable({
  providedIn: 'root'
})
export class DialogConfirmationDeleteService {

  private modal!: MatDialogRef<DialogConfirmationDeleteComponent, any>;

  constructor(public dialog: MatDialog) { }

  open(msg: string) {
    this.modal = this.dialog.open(DialogConfirmationDeleteComponent, {
      data: { msg },
      width: '40%',
      enterAnimationDuration: "200ms",
      exitAnimationDuration: "200ms",
    });
    return this.modal;
  }

  close(flag: boolean = false) {
    if (this.modal) this.modal.close(flag);
  }
}

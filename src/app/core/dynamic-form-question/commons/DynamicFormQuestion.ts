import { ValidatorFn } from "@angular/forms";

export class DynamicFormQuestion {
  key: string = '';
  value: any;
  label: string = '';
  required?: boolean = false;
  disabled?: boolean;
  options?: DynamicFormQuestionOptions[];
  validators?: ValidatorFn[]
}

export interface DynamicFormQuestionOptions {
  key: any;
  value: string;
}

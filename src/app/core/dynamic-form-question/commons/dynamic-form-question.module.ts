import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DynamicFormQuestionService } from '../services/dynamic-form-question.service';

@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  providers: [DynamicFormQuestionService]
})
export class DynamicFormQuestionModule { }

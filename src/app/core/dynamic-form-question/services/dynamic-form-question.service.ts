import { Injectable } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { DynamicFormQuestion } from '../commons/DynamicFormQuestion';

@Injectable({
  providedIn: 'root'
})
export class DynamicFormQuestionService {

  constructor() { }

  toFormGroup(questions: DynamicFormQuestion[]) {
    const group: any = {};
    questions.forEach((question) => {
      group[question.key] = new FormControl(
        { value: question.value, disabled: question.disabled },
        (question.validators && question.validators.length > 0)
          ? Validators.compose(question.required ? [Validators.required].concat(question.validators) : question.validators)
          : question.required ? Validators.required : [],
      )
    });
    return new FormGroup(group);
  }
}

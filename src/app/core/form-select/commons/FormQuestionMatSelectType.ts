export interface FormQuestionMatSelectType {
  value: any;
  key: string;
  label: string;
  required?: boolean;
  options?: { key: string; value: string }[];
}

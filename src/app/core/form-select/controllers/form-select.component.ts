import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MatFormFieldAppearance } from '@angular/material/form-field';
import { FormQuestionMatSelectType } from '../commons/FormQuestionMatSelectType';

@Component({
  selector: 'app-form-select',
  templateUrl: '../view/form-select.component.html',
  styleUrls: ['../view/form-select.component.scss']
})
export class FormSelectComponent implements OnInit {

  @Input()
  form!: FormGroup;

  @Input()
  question!: FormQuestionMatSelectType;

  @Input()
  appearance: MatFormFieldAppearance = "outline";

  constructor() { }

  ngOnInit(): void {
  }

}

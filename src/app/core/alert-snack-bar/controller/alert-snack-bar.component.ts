import { Component, Inject, Input, OnInit } from '@angular/core';
import { MAT_SNACK_BAR_DATA } from '@angular/material/snack-bar';

@Component({
  selector: 'app-alert-snack-bar',
  templateUrl: '../view/alert-snack-bar.component.html',
  styleUrls: ['../view/alert-snack-bar.component.scss']
})
export class AlertSnackBarComponent implements OnInit {

  flag: boolean = false;

  constructor(@Inject(MAT_SNACK_BAR_DATA) public data: { msg: string; code: number }) { }

  ngOnInit(): void {
    this.flag = this.data.code === 200 || this.data.code === 201;
  }

}

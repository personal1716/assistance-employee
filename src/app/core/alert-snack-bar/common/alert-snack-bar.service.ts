import { Injectable } from '@angular/core';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { AlertSnackBarComponent } from '../controller/alert-snack-bar.component';

@Injectable({
  providedIn: 'root'
})
export class AlertSnackBarService {

  private durationInSeconds = 5;
  private horizontalPosition: MatSnackBarHorizontalPosition = 'end';
  private verticalPosition: MatSnackBarVerticalPosition = 'top';

  constructor(private _snackBar: MatSnackBar) { }

  open(msg: string = "Proceso realizado correctamente.", code: number) {
    this._snackBar.openFromComponent(AlertSnackBarComponent, {
      data: {
        msg,
        code
      },
      duration: this.durationInSeconds * 1000,
      horizontalPosition: this.horizontalPosition,
      verticalPosition: this.verticalPosition,
    });
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AlertSnackBarComponent } from '../controller/alert-snack-bar.component';
import { AlertSnackBarService } from './alert-snack-bar.service';
import { MatIconModule } from '@angular/material/icon';
import { MatSnackBarModule } from '@angular/material/snack-bar';

@NgModule({
  declarations: [AlertSnackBarComponent],
  imports: [
    CommonModule,
    MatIconModule,
    MatSnackBarModule
  ], exports: [AlertSnackBarComponent],
  providers: [AlertSnackBarService]
})
export class AlertSnackBarModule { }

export default interface DocumentTypeInterface {
  id: string;
  name: string;
  abbreviation: string;
}

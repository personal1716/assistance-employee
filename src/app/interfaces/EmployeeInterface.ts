export default interface EmployeeInterface {
  id: string;
  firstName: string;
  otherName?: string;
  firstLastName: string;
  secondLastName: string;
  countryEmployment: string;
  documentType: string;
  document: string;
  email?: string;
  dateEntry?: string;
  area: string;
  createdAt: string;
  updatedAt?: string;
  status: string;
  updateEmail?: boolean;
}

import DocumentTypeInterface from "./DocumentTypeInterface";
import EmployeeInterface from "./EmployeeInterface";

export interface ResponseHttpEmployee {
  status: number;
  message: string;
  data: EmployeeInterface[] | EmployeeInterface;
}

export interface ResponseHttpDocumentType {
  status: number;
  message: string;
  data?: DocumentTypeInterface[];
}

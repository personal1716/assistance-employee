import { Injectable } from '@angular/core';
import { Validators } from '@angular/forms';
import * as moment from 'moment';
import { DynamicFormQuestion, DynamicFormQuestionOptions } from 'src/app/core/dynamic-form-question/commons/DynamicFormQuestion';
import { CustomValidationsService } from 'src/app/core/custom-validations/common/custom-validations.service';

@Injectable({
  providedIn: 'root'
})
export class EmployeeFormService {

  areas: DynamicFormQuestionOptions[] = [
    { key: 'ADMINISTRACIÓN', value: 'ADMINISTRACIÓN' },
    { key: 'FINANCIERA', value: 'FINANCIERA' },
    { key: 'COMPRAS', value: 'COMPRAS' },
    { key: 'INFRAESTRUCTURA', value: 'INFRAESTRUCTURA' },
    { key: 'OPERACIÓN', value: 'OPERACIÓN' },
    { key: 'TALENTO HUMANO', value: 'TALENTO HUMANO' },
    { key: 'SERVICIOS VARIOS', value: 'SERVICIOS VARIOS' },
  ];

  countries: DynamicFormQuestionOptions[] = [
    { key: 'CO', value: 'COLOMBIA' },
    { key: 'USA', value: 'ESTADOS UNIDOS' },
  ];

  status: DynamicFormQuestionOptions[] = [
    { key: 'ACTIVO', value: 'ACTIVO' }
  ];

  constructor(
    private customValidationsService: CustomValidationsService
  ) { }

  getFormFieldsAddEmployee(data: any = null, documentTypes: DynamicFormQuestionOptions[] = []) {
    const questions: DynamicFormQuestion[] = [
      {
        key: "documentType",
        value: data?.documentType ? data?.documentType : "",
        label: "Tipo de Documento de Identidad",
        options: documentTypes,
        required: true
      },
      {
        key: "document",
        value: data?.document ? data?.document : "",
        label: "Documento de Identidad",
        required: true,
        validators: [
          Validators.maxLength(20),
          this.customValidationsService.onlyNumberAZMinus
        ]
      },
      {
        key: "firstName",
        value: data?.firstName ? data?.firstName : "",
        label: "Primer Nombre",
        required: true,
        validators: [
          Validators.maxLength(20),
          this.customValidationsService.onlyAZ
        ]
      },
      {
        key: "otherName",
        value: data?.otherName ? data?.otherName : "",
        label: "Otros Nombres",
        validators: [
          Validators.maxLength(50),
          this.customValidationsService.onlyAZSpace
        ]
      },
      {
        key: "firstLastName",
        value: data?.firstLastName ? data?.firstLastName : "",
        label: "Primer Apellido",
        required: true,
        validators: [
          Validators.maxLength(20),
          this.customValidationsService.onlyAZ
        ]
      },
      {
        key: "secondLastName",
        value: data?.secondLastName ? data?.secondLastName : "",
        label: "Segundo Apellido",
        required: true,
        validators: [
          Validators.maxLength(20),
          this.customValidationsService.onlyAZ
        ]
      },
      {
        key: "countryEmployment",
        value: data?.countryEmployment ? data?.countryEmployment : "",
        label: "País del Empleo",
        options: this.countries,
        required: true
      },
      {
        key: "dateEntry",
        value: data?.dateEntry ? data?.dateEntry : "",
        label: "Fecha de Ingreso",
        disabled: true
      },
      {
        key: "area",
        value: data?.area ? data?.area : "",
        label: "Área",
        options: this.areas,
        required: true
      },
      {
        key: "status",
        value: data?.status ? data?.status : "ACTIVO",
        label: "Estado",
        options: this.status
      },
    ];
    return questions;
  }

  getFormFieldsEditEmployee(data: any = null, documentTypes: DynamicFormQuestionOptions[] = []) {
    const questions: DynamicFormQuestion[] = [
      {
        key: "id",
        value: data?.id ? data?.id : "",
        label: "Id",
      },
      {
        key: "documentType",
        value: data?.documentType ? data?.documentType : "",
        label: "Tipo de Documento de Identidad",
        options: documentTypes,
        required: true
      },
      {
        key: "document",
        value: data?.document ? data?.document : "",
        label: "Documento de Identidad",
        required: true,
        validators: [
          Validators.maxLength(20),
          this.customValidationsService.onlyNumberAZMinus
        ]
      },
      {
        key: "firstName",
        value: data?.firstName ? data?.firstName : "",
        label: "Primer Nombre",
        required: true,
        validators: [
          Validators.maxLength(20),
          this.customValidationsService.onlyAZ
        ]
      },
      {
        key: "otherName",
        value: data?.otherName ? data?.otherName : "",
        label: "Otros Nombres",
        validators: [
          Validators.maxLength(50),
          this.customValidationsService.onlyAZSpace
        ]
      },
      {
        key: "firstLastName",
        value: data?.firstLastName ? data?.firstLastName : "",
        label: "Primer Apellido",
        required: true,
        validators: [
          Validators.maxLength(20),
          this.customValidationsService.onlyAZ
        ]
      },
      {
        key: "secondLastName",
        value: data?.secondLastName ? data?.secondLastName : "",
        label: "Segundo Apellido",
        required: true,
        validators: [
          Validators.maxLength(20),
          this.customValidationsService.onlyAZ
        ]
      },
      {
        key: "countryEmployment",
        value: data?.countryEmployment ? data?.countryEmployment : "",
        label: "País del Empleo",
        options: this.countries,
        required: true
      },
      {
        key: "dateEntry",
        value: data?.dateEntry ? moment(data?.dateEntry, "DD/MM/YYYY") : "",
        label: "Fecha de Ingreso",
        disabled: true
      },
      {
        key: "area",
        value: data?.area ? data?.area : "",
        label: "Área",
        options: this.areas,
        required: true
      },
      {
        key: "status",
        value: data?.status ? data?.status : "ACTIVO",
        label: "Estado",
        disabled: true,
        options: this.status
      },
      {
        key: "createdAt",
        value: data?.createdAt ? data?.createdAt : "",
        label: "Fecha y Hora de registro",
      },
      {
        key: "updatedAt",
        value: data?.updatedAt ? data?.updatedAt : "",
        label: "Fecha y Hora de edición",
      },
    ];
    return questions;
  }
}

import { HttpClient } from "@angular/common/http";
import { Injectable } from '@angular/core';
import { firstValueFrom } from 'rxjs';
import {ResponseHttpDocumentType} from "../interfaces/ResponseHttpType";

@Injectable({
  providedIn: 'root'
})
export class DocumentTypeService {

  private URL = "http://localhost:3000/api/documentType";

  constructor(private http: HttpClient) { }

  getAllDocumentTypes() {
    return firstValueFrom<ResponseHttpDocumentType>(this.http.get<ResponseHttpDocumentType>(this.URL));
  }
}

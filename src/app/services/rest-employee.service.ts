import { HttpClient, HttpParams } from "@angular/common/http";
import { Injectable } from '@angular/core';
import { firstValueFrom } from 'rxjs';
import EmployeeInterface from "../interfaces/EmployeeInterface";
import { ResponseHttpEmployee } from "../interfaces/ResponseHttpType";

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  private URL = "http://localhost:3000/api/employee";

  constructor(private http: HttpClient) { }

  addEmployee(data: EmployeeInterface) {
    return firstValueFrom<ResponseHttpEmployee>(this.http.post<ResponseHttpEmployee>(this.URL, data));
  }

  getAllEmployees() {
    return firstValueFrom<ResponseHttpEmployee>(this.http.get<ResponseHttpEmployee>(this.URL));
  }

  getEmployeeById(id: string) {
    const params = new HttpParams().set('id', id);
    return firstValueFrom<ResponseHttpEmployee>(this.http.get<ResponseHttpEmployee>(`${this.URL}/doc`, { params }));
  }

  updateEmployee(id: string, data: EmployeeInterface) {
    return firstValueFrom<ResponseHttpEmployee>(this.http.put<ResponseHttpEmployee>(this.URL, { id, data }));
  }

  deleteEmployee(id: string) {
    return firstValueFrom<ResponseHttpEmployee>(this.http.delete<ResponseHttpEmployee>(this.URL, { body: { id } }));
  }
}

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: "", pathMatch: "full", redirectTo: "home" },
  {
    path: "",
    children: [
      {
        path: "home",
        loadChildren: () => import("./components/employee/list-employee/common/list-employee.module").then((m) => m.ListEmployeeModule)
      },
    ],
  },
  {
    path: "home",
    children: [
      {
        path: "add",
        loadChildren: () => import("./components/employee/add-employee/common/add-employee.module").then((m) => m.AddEmployeeModule)
      },
      {
        path: "edit",
        loadChildren: () => import("./components/employee/edit-employee/common/edit-employee.module").then((m) => m.EditEmployeeModule)
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

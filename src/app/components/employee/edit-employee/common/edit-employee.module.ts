import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EditEmployeeRoutingModule } from './edit-employee-routing.module';
import { EditEmployeeComponent } from '../controller/edit-employee.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { AlertSnackBarModule } from 'src/app/core/alert-snack-bar/common/alert-snack-bar.module';
import { DynamicFormQuestionModule } from 'src/app/core/dynamic-form-question/commons/dynamic-form-question.module';
import { FormDatePickerModule } from 'src/app/core/form-date-picker/commons/form-date-picker.module';
import { FormInputModule } from 'src/app/core/form-input/commons/form-input.module';
import { FormSelectModule } from 'src/app/core/form-select/commons/form-select.module';

@NgModule({
  declarations: [EditEmployeeComponent],
  imports: [
    CommonModule,
    EditEmployeeRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    FormInputModule,
    FormSelectModule,
    DynamicFormQuestionModule,
    MatProgressSpinnerModule,
    FormDatePickerModule,
    MatButtonModule,
    AlertSnackBarModule,
    MatIconModule
  ], exports: [EditEmployeeComponent]
})
export class EditEmployeeModule { }

import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import * as moment from 'moment';
import { AlertSnackBarService } from 'src/app/core/alert-snack-bar/common/alert-snack-bar.service';
import { DynamicFormQuestion } from 'src/app/core/dynamic-form-question/commons/DynamicFormQuestion';
import { DynamicFormQuestionService } from 'src/app/core/dynamic-form-question/services/dynamic-form-question.service';
import DocumentTypeInterface from 'src/app/interfaces/DocumentTypeInterface';
import EmployeeInterface from 'src/app/interfaces/EmployeeInterface';
import { ResponseHttpDocumentType, ResponseHttpEmployee } from 'src/app/interfaces/ResponseHttpType';
import { DocumentTypeService } from 'src/app/services/rest-document-type.service';
import { EmployeeService } from 'src/app/services/rest-employee.service';
import { EmployeeFormService } from '../../../../services/forms/employee-form.service';

@Component({
  selector: 'app-edit-employee',
  templateUrl: '../view/edit-employee.component.html',
  styleUrls: ['../view/edit-employee.component.scss']
})
export class EditEmployeeComponent implements OnInit {

  form!: FormGroup;

  questions!: DynamicFormQuestion[];

  flag: boolean = false;

  private dataCopy!: EmployeeInterface;

  private id: string | null = "";

  constructor(
    private dynamicFormQuestionService: DynamicFormQuestionService,
    private employeeFormService: EmployeeFormService,
    private documentTypeService: DocumentTypeService,
    private employeeService: EmployeeService,
    private alertSnackBarService: AlertSnackBarService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.id = sessionStorage.getItem('idEditEmployee');
    if (!this.id) {
      this.router.navigateByUrl("/home");
      return;
    }
    this.loadForm(this.id);
  }

  async loadForm(id: string) {
    const listDocumentTypes: ResponseHttpDocumentType = await this.documentTypeService.getAllDocumentTypes();
    const arrayDocumentTypes = listDocumentTypes.data?.map((docType: DocumentTypeInterface) => {
      return { key: docType.id, value: docType.name }
    })

    const data: ResponseHttpEmployee = await this.employeeService.getEmployeeById(id);
    this.dataCopy = data.data as EmployeeInterface;

    this.questions = this.employeeFormService.getFormFieldsEditEmployee(data.data, arrayDocumentTypes);
    this.form = this.dynamicFormQuestionService.toFormGroup(this.questions);
    this.flag = true;
  }

  async onClickSave() {
    this.flag = false;

    const form = this.form.getRawValue();
    const id = form.id;
    delete form.id;

    form.dateEntry = moment(form.dateEntry).format('DD/MM/YYYY');

    if (form.firstName !== this.dataCopy.firstName ||
      form.firstLastName !== this.dataCopy.firstLastName ||
      form.countryEmployment !== this.dataCopy.countryEmployment) {
      form.updateEmail = true
    }

    const resp: ResponseHttpEmployee = await this.employeeService.updateEmployee(id, form);

    this.alertSnackBarService.open(resp.message, resp.status);
    if (resp && resp.status === 200) {
      await this.loadForm(this.questions[0].value);
    }
    this.flag = true;
  }
}

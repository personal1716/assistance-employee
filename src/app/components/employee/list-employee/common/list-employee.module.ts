import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListEmployeeRoutingModule } from './list-employee-routing.module';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatFormFieldModule } from '@angular/material/form-field';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ListEmployeeComponent } from '../controller/list-employee.component';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { DialogConfirmationDeleteModule } from 'src/app/core/dialog-confirmation-delete/common/dialog-confirmation-delete.module';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { AlertSnackBarModule } from 'src/app/core/alert-snack-bar/common/alert-snack-bar.module';
import { MatToolbarModule } from '@angular/material/toolbar';


@NgModule({
  declarations: [ListEmployeeComponent],
  imports: [
    CommonModule,
    ListEmployeeRoutingModule,
    MatTableModule,
    MatPaginatorModule,
    MatFormFieldModule,
    MatInputModule,
    ReactiveFormsModule,
    FormsModule,
    MatButtonModule,
    MatIconModule,
    DialogConfirmationDeleteModule,
    MatProgressSpinnerModule,
    AlertSnackBarModule,
    MatToolbarModule
  ], exports: [ListEmployeeComponent]
})
export class ListEmployeeModule { }

import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { AlertSnackBarService } from 'src/app/core/alert-snack-bar/common/alert-snack-bar.service';
import { DialogConfirmationDeleteService } from 'src/app/core/dialog-confirmation-delete/common/dialog-confirmation-delete.service';
import EmployeeInterface from 'src/app/interfaces/EmployeeInterface';
import { ResponseHttpEmployee } from 'src/app/interfaces/ResponseHttpType';
import { EmployeeService } from 'src/app/services/rest-employee.service';
@Component({
  selector: 'app-list-employee',
  templateUrl: '../view/list-employee.component.html',
  styleUrls: ['../view/list-employee.component.scss']
})
export class ListEmployeeComponent implements OnInit {
  displayedColumns: { key: string, value: string }[] = [
    { key: 'firstName', value: 'Primer Nombre' },
    { key: 'otherName', value: 'Otros Nombres' },
    { key: 'firstLastName', value: 'Primer Apellido' },
    { key: 'secondLastName', value: 'Segundo Apellido' },
    { key: 'countryEmployment', value: 'País Empleo' },
    { key: 'documentType', value: 'Tipo Documento' },
    { key: 'document', value: 'Documento' },
    { key: 'email', value: 'Correo' },
    { key: 'dateEntry', value: 'Fecha Ingreso' },
    { key: 'area', value: 'Área' },
    { key: 'createdAt', value: 'Fecha/Hora Registro' },
    { key: 'updatedAt', value: 'Fecha/Hora Edición' },
  ];

  headerColumns: string[] = [];

  dataSource!: MatTableDataSource<EmployeeInterface>;

  @ViewChild(MatPaginator) paginator!: MatPaginator;

  @ViewChild(MatSort)
  sort: MatSort = new MatSort;

  flagTable: boolean = true;

  pageSizeOptions: number[] = [10, 25, 50];

  constructor(
    private employeeService: EmployeeService,
    private cdr: ChangeDetectorRef,
    private dialogConfirmationDeleteService: DialogConfirmationDeleteService,
    private alertSnackBarService: AlertSnackBarService,
    private router: Router
  ) {
  }

  ngOnInit() {
    this.initConsults();
  }

  initConsults() {
    sessionStorage.removeItem('idEditEmployee');
    this.loadHeaderColumns();
    this.consultEmployees();
  }

  loadHeaderColumns() {
    this.headerColumns = this.displayedColumns.map(col => col.key);
    this.headerColumns.push('actions');
  }

  async consultEmployees() {
    const listEmployees: ResponseHttpEmployee = await this.employeeService.getAllEmployees();
    this.dataSource = new MatTableDataSource(listEmployees.data as EmployeeInterface[]);
    this.flagTable = false;
    this.cdr.detectChanges();
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  onClickDelete(id: string) {
    const msg = '¿Está seguro(a) de que desea eliminar el empleado?';
    const modal = this.dialogConfirmationDeleteService.open(msg);
    modal.afterClosed().subscribe((result: boolean) => {
      if (result) this.deleteEmployee(id);
    });
  }

  async deleteEmployee(id: string) {
    this.flagTable = true;
    const resp: ResponseHttpEmployee = await this.employeeService.deleteEmployee(id);
    this.alertSnackBarService.open(resp.message, resp.status);
    if (resp && resp.status === 200) this.initConsults();
  }

  onClickEdit(id: string) {
    sessionStorage.setItem('idEditEmployee', id);
    this.router.navigateByUrl("/home/edit");
  }

  search(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}

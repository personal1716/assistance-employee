import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { AddEmployeeComponent } from '../controller/add-employee.component';
import { AddEmployeeRoutingModule } from './add-employee-routing.module';
import { FormInputModule } from 'src/app/core/form-input/commons/form-input.module';
import { DynamicFormQuestionModule } from 'src/app/core/dynamic-form-question/commons/dynamic-form-question.module';
import { EmployeeFormService } from '../../../../services/forms/employee-form.service';
import { FormSelectModule } from 'src/app/core/form-select/commons/form-select.module';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { FormDatePickerModule } from 'src/app/core/form-date-picker/commons/form-date-picker.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { AlertSnackBarModule } from 'src/app/core/alert-snack-bar/common/alert-snack-bar.module';
import { MatIconModule } from '@angular/material/icon';

@NgModule({
  declarations: [AddEmployeeComponent],
  imports: [
    CommonModule,
    AddEmployeeRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    FormInputModule,
    FormSelectModule,
    DynamicFormQuestionModule,
    MatProgressSpinnerModule,
    FormDatePickerModule,
    MatButtonModule,
    AlertSnackBarModule,
    MatIconModule
  ], exports: [AddEmployeeComponent],
  providers: [EmployeeFormService]
})
export class AddEmployeeModule { }

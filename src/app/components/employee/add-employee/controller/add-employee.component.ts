import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import * as moment from 'moment';
import { AlertSnackBarService } from 'src/app/core/alert-snack-bar/common/alert-snack-bar.service';
import { DynamicFormQuestion } from 'src/app/core/dynamic-form-question/commons/DynamicFormQuestion';
import { DynamicFormQuestionService } from 'src/app/core/dynamic-form-question/services/dynamic-form-question.service';
import DocumentTypeInterface from 'src/app/interfaces/DocumentTypeInterface';
import { ResponseHttpDocumentType, ResponseHttpEmployee } from 'src/app/interfaces/ResponseHttpType';
import { DocumentTypeService } from 'src/app/services/rest-document-type.service';
import { EmployeeService } from 'src/app/services/rest-employee.service';
import { EmployeeFormService } from '../../../../services/forms/employee-form.service';

@Component({
  selector: 'app-add-employee',
  templateUrl: '../view/add-employee.component.html',
  styleUrls: ['../view/add-employee.component.scss']
})
export class AddEmployeeComponent implements OnInit {

  form!: FormGroup;

  questions!: DynamicFormQuestion[];

  flag: boolean = false;

  constructor(
    private dynamicFormQuestionService: DynamicFormQuestionService,
    private employeeFormService: EmployeeFormService,
    private documentTypeService: DocumentTypeService,
    private employeeService: EmployeeService,
    private alertSnackBarService: AlertSnackBarService
  ) { }

  ngOnInit(): void {
    this.loadForm();
  }

  async loadForm() {
    const listDocumentTypes: ResponseHttpDocumentType = await this.documentTypeService.getAllDocumentTypes();
    const arrayDocumentTypes = listDocumentTypes.data?.map((docType: DocumentTypeInterface) => {
      return { key: docType.id, value: docType.name }
    })
    this.questions = this.employeeFormService.getFormFieldsAddEmployee(null, arrayDocumentTypes);
    this.form = this.dynamicFormQuestionService.toFormGroup(this.questions);
    this.flag = true;
  }

  async onClickSave() {
    this.flag = false;

    const form = this.form.getRawValue();
    form.dateEntry = moment(form.dateEntry).format('DD/MM/YYYY');
    
    const resp: ResponseHttpEmployee = await this.employeeService.addEmployee(form);
    
    this.alertSnackBarService.open(resp.message, resp.status);
    if (resp && resp.status === 201) {
      await this.loadForm();
    }
    this.flag = true;
  }
}
